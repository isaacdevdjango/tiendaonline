<?php

use Illuminate\Support\Facades\Route;
use App\Models\Producto;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['uses'=>'App\Http\Controllers\ProductoController@getWelcome',
				'as'=> 'producto.welcome']);

Route::get('/checkout',['uses'=>'App\Http\Controllers\ProductoController@getCheckout',
				'as'=> 'producto.checkout']);

Route::post('/checkout',['uses'=>'App\Http\Controllers\ProductoController@postCheckout',
				'as'=> 'producto.checkout']);


Route::get('/index',['uses'=>'App\Http\Controllers\ProductoController@getIndex',
				'as'=> 'producto.index']);




Route::get('editarProducto',['uses'=>'App\Http\Controllers\ProductoController@getEditarProducto',
				'as'=> 'producto.editarProducto',
				'middleware' => 'auth']);

Route::post('editarProducto',['uses'=>'App\Http\Controllers\ProductoController@postEditarProducto',
				'as'=> 'producto.editarProducto',
				'middleware' => 'auth']);



Route::post('editarProducto','App\Http\Controllers\ProductoController@editarProducto');

Route::get('/signup',['uses'=>'App\Http\Controllers\UserController@getSignup',
					'as'=> 'user.signup',
					'middleware' => 'guest']);

Route::post('/signup',['uses'=> 'App\Http\Controllers\UserController@postSignup',
					'as'=> 'user.signup',
				'middleware' => 'guest']);

Route::get('/signin',['uses'=>'App\Http\Controllers\UserController@getSignin',
					'as'=> 'user.signin',
				'middleware' => 'guest']);

Route::post('/signin',['uses'=> 'App\Http\Controllers\UserController@postSignin',
					'as'=> 'user.signin',
				'middleware' => 'guest']);

Route::get('/user/profile',['uses'=>'App\Http\Controllers\UserController@getProfile',
					'as'=> 'user.profile',
					'middleware' => 'auth']);

Route::get('/user/logout',['uses'=>'App\Http\Controllers\UserController@getLogout',
					'as'=> 'user.logout',
					'middleware' => 'auth']);
Route::get('/user/deletecart',['uses'=>'App\Http\Controllers\ProductoController@deleteCart',
					'as'=> 'user.deletecart',
					]);


Route::get('/addtocart/{id}',['uses'=>'App\Http\Controllers\ProductoController@getAddtoCart',
					'as'=> 'producto.addtocart']);

Route::get('/shoppingCart',['uses'=>'App\Http\Controllers\ProductoController@getCart',
					'as'=> 'producto.shoppingCart']);



#Route::get('/',[ProductoController::class, 'test']);


/*Route::get('productos', function () {
	$producto= Producto::All();
	foreach($producto as $item){

		echo '<p>'. $item->nombre_producto . 'con categoria' . $item->Categoria->nombreCategoria . '</p>';

	}
  #  return response()->json($producto);
});
*/