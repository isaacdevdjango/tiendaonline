<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Producto;



class ProductoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/**
      DB::table('productos')->insert([
        	'imagePath' =>'images/bottleWater1.jpeg',
        	'nombre_producto'=> 'Botella',
        	'precio'=> 95,
        	'costo'=> 70,
        	'descripcion'=> 'Botella para Agua',
        	'categoria_id'=> 1
        ]);*/
        $producto= new Producto([
        	'imagePath' =>'images/bottleWater1.jpeg',
        	'nombre_producto'=> 'Botella2',
        	'precio'=> 100,
        	'costo'=> 70,
        	'descripcion'=> 'Botella para Agua',
        	'categoria_id'=> 1
        ]); 
         $producto -> save();
    }
}
