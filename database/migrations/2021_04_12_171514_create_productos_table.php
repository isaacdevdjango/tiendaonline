<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table -> string('imagePath');
            $table -> string ('nombre_producto');
            $table -> float ('precio')->default('1.0');
            $table -> float ('costo')->default('1.0');
            $table -> string ('descripcion');
            $table -> unsignedBigInteger ('categoria_id')->default('1');
            $table -> boolean ('activo')->default(true); 
            });           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
