<nav class="navbar navbar-dark bg-dark" aria-label="First navbar example">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Veterinaria Shoppping Online</a>
      <a class="navbar-brand pull-right" href="{{route ('producto.shoppingCart')}}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
  <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
</svg> <span class="badge">{{Session::has('cart') ? Session::get('cart')->totalQty :'' }} </span> </a>
@if(Auth::check())
<span class="badge">welcome: {{Auth::user()-> name}}</span>
@endif
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample01">
        <ul class="navbar-nav me-auto mb-2">
          @if(Auth::check())
          
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="{{ route('user.profile') }}">user profile</a>
                </li>
                <li class="nav-item">
                <a class="nav-link " aria-current="page" href="{{ route('user.logout') }}">Logout</a>
              </li>

          @else
                <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="{{ route('user.signin') }}">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active"  href="{{ route('user.signup') }}">Registrate <i class="fas fa-address-book"></i></a>
              </li>
              
          @endif


          
          <li class="nav-item">
 
          <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false">Dropdown</a>
            <ul class="dropdown-menu" aria-labelledby="dropdown01">
              <li><a class="dropdown-item" href="#">Action</a></li>
              <li><a class="dropdown-item" href="#">Another action</a></li>
              <li><a class="dropdown-item" href="#">Something else here</a></li>
            </ul>
          </li>
        </ul>
        <form>
          <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
      </div>
    </div>
  </nav>