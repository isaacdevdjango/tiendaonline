<div class=" nav flex-column  text-white " style="float:left;  margin:0 auto; width: 280px; height:600px">
  <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
    <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
    <span class="fs-4">Sidebar</span>
  </a>
  <hr>
  <ul class="nav nav-pills flex-column mb-auto">
    <li class="nav-item"> 
      <a href="#" id="aInicio" class="nav-link active">
        <svg class="bi me-2" width="16" height="16"><use xlink:href="#add"></use></svg>
        Home
      </a>
    </li>
    @if(Auth::check())
      @if(Auth::user()->id ==6)

    <li>
      <a href="{{route('producto.editarProducto')}}" id="aEditarProducto"class="nav-link  text-white">
        <svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg>
        Editar Producto
      </a>
    </li>
    @endif
    @endif
    <li>
      <a href="{{route('user.profile')}}" class="nav-link text-white">
        <svg class="bi me-2" width="16" height="16"><use xlink:href="#table"></use></svg>
        Orders
      </a>
    </li>
    <li>
      <a href="{{route('producto.index')}}" class="nav-link text-white">
        <svg class="bi me-2" width="16" height="16"><use xlink:href="#grid"></use></svg>
        Products
      </a>
    </li>
  </ul>
  <hr>
  
</div>