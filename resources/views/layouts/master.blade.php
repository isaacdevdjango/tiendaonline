<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title> @yield('title') </title>
  
      <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css"> 
     
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  @yield('styles')
      
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
      </script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js">  
      </script>
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
      <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

      	
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script >
     

       $(document).ready(function () {

        //side bar functionality
        $("#aInicio").click(function(){
        
          $("#aInicio").removeClass("active");
          $("#aEditarProducto").removeClass("active");
          $("#aInicio").addClass("active");
          $(location).attr('href', 'http://veterinariashop.test/');
        });
        $("#aEditarProducto").click(function(){
          $("#aInicio").removeClass("active");
          $("#aEditarProducto").removeClass("active");
          $("#aEditarProducto").addClass("active");
          $(location).attr('href', 'http://veterinariashop.test/editarProducto');

        });
        
        //sign up submit form 
        $("#btnSubmitsignup").click(function () {
       
        returntype=validateSignupFields();
        if (returntype==true){
          $("#formSignup").submit();

        }
      

        });
  
        //checkout form 
        $("#formComprarBtn").click(function () {
          
          $("#formCheckout").submit();

        });
  

        $("#btnEliminaTodos").click(function () {
      
          

          $(location).attr('href', 'http://veterinariashop.test/user/deletecart');
        });

  $("#btnSubmitformSignin").click(function () {
        
    returntype=validateSigninFields();
        if (returntype==true){
         $("#formSignin").submit();
      }

        });


        });

      function validateSigninFields(){

returntype=true;
        email=document.forms["formSignin"]["email"].value;
        password=document.forms["formSignin"]["password"].value;
       
                   
         if (email == "" && typeof(email)!="number" ) {
            document.getElementById("email").style.border="thick solid #0000FF"
             returntype = false;
            
         }
         if (typeof(email)=="number" ) {
            document.getElementById("email").style.border="thick solid #0000FF"
             returntype = false;
            
         }
          if (password == "" && typeof(password)!="number" ) {
            document.getElementById("password").style.border="thick solid #0000FF"
             returntype = false;
            
         }
         if ( typeof(password)=="number" ) {
            document.getElementById("password").style.border="thick solid #0000FF"
             returntype = false;
            
         }
        
         return(returntype);

  }
  function validateSignupFields(){

returntype=true;
        nombre=document.forms["formSignup"]["name"].value;
        email=document.forms["formSignup"]["email"].value;
        password=document.forms["formSignup"]["password"].value;
       
        if (nombre == "" && typeof(nombre)!="number" ) {
            document.getElementById("name").style.border="thick solid #0000FF"
            returntype = false;
            
        }
         if (typeof(nombre)=="number" ) {
            document.getElementById("name").style.border="thick solid #0000FF"
            returntype = false;
                    }
         if (email == "" && typeof(email)!="number" ) {
            document.getElementById("email").style.border="thick solid #0000FF"
             returntype = false;
            
         }
         if (typeof(email)=="number" ) {
            document.getElementById("email").style.border="thick solid #0000FF"
             returntype = false;
            
         }
          if (password == "" && typeof(password)!="number" ) {
            document.getElementById("password").style.border="thick solid #0000FF"
             returntype = false;
            
         }
         if ( typeof(password)=="number" ) {
            document.getElementById("password").style.border="thick solid #0000FF"
             returntype = false;
            
         }
        
         return(returntype);

  }

  @yield('scripts')

    </script>


      <style>

        </style>

</head>
<body>
@include('partials.header')
<div  class="container-fluid">
  <div  class="row">
    <div style="background: black;" class="col-sm-3 sidenav">
       @include('partials.sidebar')
    </div>
  <div class="col">
      @yield('content')
   </div> 
  </div>
</div>


</body>
</html>