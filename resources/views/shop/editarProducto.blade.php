@extends('layouts.master')

@section('title')
	Veterinaria Shoppping Cart
@endsection
<style>
.card-img-top	{
	height:350px;

}
.price{

	font-weight: bold;
}

</style>

@section('content')
<div  class="container-fluid">

<h1>Prueba</h1>

<div class="col-sm-9" >
             <div class="well" style="height:auto;  overflow:auto;">
                 <h1>Agregar un Producto</h1>
                <form id="agregarProducto" name="agregarProducto" action="{{ route('producto.editarProducto') }}" method="POST">
             
              <div class="form-group">
                    <label for="NombreProducto">Nombre del Producto</label>
                    <input type="text" class="form-control" name="NombreProducto" id="NombreProducto" placeholder="Producto">
              </div>
             
               
            <div class="form-group">
            <label for="inputCosto">Costo del prodcuto</label>
            <input type="text" class="form-control" name="inputCosto" id="inputCosto" placeholder="Costo">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Precio</label>
            <input type="text" class="form-control" name="precioProducto" id="precioProducto" placeholder="precio">
          </div>
             <label for="exampleInputPassword1">Descripcion</label>
          <textarea id="DescripcionProducto" name="DescripcionProducto" class="form-control" rows="3"></textarea>
            <br />
            @if (count($categorias) > 0)  
            <select id="categoriaoption" name="categoriaoption" class="form-control">
                @foreach($categorias as $categoria)
                    <option value="" selected disabled hidden>escoje opcion</option>
                    <option value="{{$categoria->id}}">{{$categoria->nombreCategoria}}</option>
               @endforeach
            </select>
                
            @endif
             <br />
          <a class="btn btn-primary" id="addProd">Registrar</a>
           {{ csrf_field() }}
        </form>

      </div>

</div>
  
@endsection

