@extends('layouts.master')

@section('title')
	Veterinaria Shoppping Cart
@endsection

<style>
.card-img-top	{
	height:350px;

}
.price{

	font-weight: bold;
}

</style>


@section('content')
<div  class="container-fluid">
		  <div  class="row">
						<div class="col">
						 <div class="card" style="width:50%;">
							 
							  <div class="card-body">
							    <p class="card-text"> check out </p>
				   			    <div class="pull-left price"style="float:left;">Total {{$total}}$ </div>
				   			    <br>
				   			    <form id="formCheckout" action="{{ route ('producto.checkout')}}" method="post">
				   			 		<div class="form-group">
									    <label for="formGroupExampleInput">Nombre</label>
									    <input type="text" class="form-control" id="formNombreInput" name="formNombreInput"placeholder="Nombre">
									  </div>
									  <div class="form-group">
									    <label for="formGroupExampleInput">direccion</label>
									    <input type="text" class="form-control" id="formDireccionInput" name="formDireccionInput" placeholder="Direecion">
									  </div>
									  <div class="form-group">
									    <label for="formGroupExampleInput">Nombre en la Tarjeta</label>
									    <input type="text" class="form-control" id="formNombreTarjetaInput" name="formNombreTarjetaInput" placeholder="Nombre en Tarjeta">
									  </div>
									  <div class="form-group">
									    <label for="formGroupExampleInput2">numero de Tarjeta de credito</label>
									    <input type="text" name="formNumTarjetaInput" class="form-control" id="formNumTarjetaInput" placeholder="numero">

									    <br/>
									  
									  </div>
									    <button  id="formComprarBtn" class="btn btn-primary">Comprar</button>
									  {{ csrf_field() }}
				   			    </form>
							    
							
						</div>
			</div>
	
</div>
  
@endsection

