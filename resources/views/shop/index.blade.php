@extends('layouts.master')

@section('title')
	Veterinaria Shoppping Cart
@endsection

<style>
.card-img-top	{
	height:350px;

}
.price{

	font-weight: bold;
}

</style>

@section('content')
<div  class="container-fluid">
	@foreach($productos->chunk(2) as $itemschunk)
		  <div  class="row">
	  		@foreach($itemschunk as $itemproduct)
						<div class="col">
						 <div class="card" style="width: 18rem;">
							  <img src="http://veterinariashop.test/{{$itemproduct->imagePath}}" class="card-img-top">
							  <div class="card-body">
							    <p class="card-text">{{$itemproduct->descripcion}}</p>
				   			    <div class="pull-left price"style="float:left;">{{$itemproduct->precio}} $</div>
							    <a href="{{ route ('producto.addtocart',['id' => $itemproduct->id ]) }}" class="btn btn-primary pull-right" style="float:right;">add to cart</a>
							  </div>
							</div>
						</div>
				@endforeach
			</div>
	@endforeach
</div>
  
@endsection

