@extends('layouts.master')

@section('title')
	Veterinaria Shoppping Cart
@endsection

<style>
.card-img-top	{
	height:350px;

}
.price{

	font-weight: bold;
}

</style>

@section('content')
<div  class="container-fluid">
		@if(Session::has('cart'))
			 <div  class="row">
			 	<div class="col">
			 		<table class="table">
			 			<tr>
					      <th scope="col">cantidad</th>
					      <th scope="col">Nombre producto</th>
					      <th scope="col">precio</th>
					      <th scope="col">elimina 1</th>
					     </thead>
					    </tr>
					     <tbody>	
					@foreach($productos as $producto )
					<tr>
			 					 <td> {{$producto['qty']}}</td>
			 					<td><strong>{{ $producto['item']['nombre_producto']  }} 
			 					</strong> </td>
			 					<td><span class="label label-success">{{$producto['precio']}}</span> </td>
			 					<div class="btn-group">
			 			
			 				<td>		<a href="">elimina un producto</a>  </td>
	 		 						
			 				</tr>

			 			@endforeach

			 		</tbody>		
				</table>

			 	</div>	

			 
			 </div>				

			 	<div  class="row">
			 			<div class="col">
					 		<strong>Total: {{ $totalPrice }} </strong>
			 			</div>

			 	</div>
			 	<div  class="row">
			 			<div class="col">
					 		<a href="{{ route ('producto.checkout')}}"  class="btn btn-success">
					 		 checkout</a>
					 		 		<button type="button" id="btnEliminaTodos" class="btn btn-primary btn-xs dropdown-toogle" data-toggle="dropdown">
			 						elimina todos
			 					</button>
			 			</div>

			 	</div>
			 		


		@else
			<div  class="row">
			 			<div class="col">
					 		<strong>no hay productos en el carrito </strong>
			 			</div>

			 	</div>
		@endif


</div>
  
@endsection

