@extends('layouts.master')

@section('title')
	Veterinaria Shoppping Cart
@endsection

@section('content')
<h2> HISTORIAL DE COMPRAS </h2>

<div  class="container-fluid">
	@foreach($orders as $order)
		  <div  class="row">
		  	<div class="col">
		  		<table class="table">
			 			<tr>
					      <th scope="col">Nombre</th>
					      <th scope="col">precio</th>
					      <th scope="col">cantidad comprada</th>
					      <th scope="col">fecha</th>
					     </thead>
					    </tr>
					     <tbody>		
	  		@foreach($order->cart->items as $item)
				<tr>
			 					 <td> {{ $item ['item']['nombre_producto'] }}</td>
			 					<td><strong>{{ $item ['item']['precio'] }}
			 					</strong> </td>
			 					<td><span class="label label-success">{{ $item ['qty']}}</span> </td>
			 					<div class="btn-group">
			 					<td>{{$order->created_at}}</td>
			 						
			 				</tr>						
						 	    	
							    	
							    
						
				@endforeach
				</div>
			</div>
	@endforeach



@endsection