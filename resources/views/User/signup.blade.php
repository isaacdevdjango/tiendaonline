@extends('layouts.master')

@section('title')
	Veterinaria Shoppping Cart
@endsection
<style>
.card-img-top	{
	height:350px;

}
.price{

	font-weight: bold;
}

</style>

@section('content')
<div  class="container-fluid">
		  <div  class="row">
				<div class="col">
					<div style="width:40%;margin: auto; ">
						  <h2>Registrate</h2>
						 <div class="form-group">
						 @if(count($errors) > 0)
						 <div class="alert alert-danger">
						 	@foreach($errors->all() as $error)
						 	<p>{{ $error }} </p>
						 	@endforeach

						 </div>
						 @endif
						  	<form id="formSignup" action="{{ route('user.signup') }}" method="post">
						   	 	<div class="form-group">
						      		<label for="name">name:</label>
						      		<input  class="form-control" id="name" placeholder="Enter name" name="name">
						   	    </div>
						   	 	<div class="form-group">
						      		<label for="email">Email:</label>
						      		<input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
						    	</div>
							    <div class="form-group">
							      	<label for="pwd">Password:</label>
							      	<input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
							    </div></br>
							    <div class="form-group">
							       
								    <a class="btn btn-primary"  id="btnSubmitsignup">submit</a>
								 </div>
								  {{ csrf_field() }}
							  	</form>
							</div>
					  </div>
				</div>
		</div>
</div>
				
			

@endsection