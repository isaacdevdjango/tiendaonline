@extends('layouts.master')

@section('title')
	Veterinaria Shoppping Cart
@endsection
<style>
.card-img-top	{
	height:350px;

}
.price{

	font-weight: bold;
}

</style>

@section('content')
<div  class="container-fluid">
		  <div  class="row">
						<div class="col">
					<div style="width:40%;margin: auto; ">
						  <h2>Login</h2>
						@if(count($errors)>0)
							<div class="alert alert-danger">
								@foreach($errors->all() as $error )
								<p>{{ $error }}</p>
								@endforeach

							</div>
						@endif
						  <div class="form-group">
						  <form id="formSignin"action="{{ route('user.signin') }}" method="post">
						    <div class="form-group">
						      <label for="email">Email:</label>
						      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
						    </div>
						    <div class="form-group">
						      <label for="pwd">Password:</label>
						      <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
						    </div></br>
						       
							   <a class="btn btn-primary"  id="btnSubmitformSignin" >Enviar</a>
							  </div>
							  {{ csrf_field() }}
						  </form>
						</div>
						  </div>
						</div>
			</div>
				
			</div>

@endsection