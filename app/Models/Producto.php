<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable =['imagePath','nombre_producto','precio','costo','descripcion','categoria_id'];
    use HasFactory;
    public function categoria(){

    	return $this -> belongsTo('App\Models\Categoria');
    }

}
