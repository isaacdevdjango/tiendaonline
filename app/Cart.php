<?php

namespace App;


class Cart
{

    public $items=null;
    public $totalQty=0;
    public $totalPrice = 0; 

    public function __construct($oldCart)
    {
        if ($oldCart){

            $this -> items = $oldCart -> items;
            $this -> totalQty = $oldCart -> totalQty;
            $this -> totalPrice = $oldCart -> totalPrice;

        }
    }

    public  function add($item, $id)
    {
            $storeditem =['qty' => 0,'precio' => $item->precio, 'item'=> $item ];  
            if ($this -> items){
                if (array_key_exists($id, $this-> items)){
                    $storeditem=$this -> items[$id];
                }
            }

            $storeditem['qty']++;
            $storeditem['precio']=$item->precio * $storeditem['qty'];
            $this -> items[$id]= $storeditem;    
            $this -> totalQty++; 
            $this -> totalPrice += $item->precio; 
    }

}
