<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use App\Models\Order;
class UserController extends Controller
{
    //
    public function getSignup(){
		return view('user.signup');

	}
	public function postSignup(Request $request){
		$this->validate($request, ['email' => 'email|required|unique:users']);    	

		$user = new User([
			'email' => $request['email'],
			'password' => bcrypt($request['password']),
			'name' => $request['name']  

		]);

		$user->save();
		return redirect()->route('producto.index'); 
	}

public function getSignin(){

	return view ('user.signin');
}

public function postSignin(Request $request){
	$this->validate ($request,[
		'email'=>'email|required',
		'password'=>'required'
	]);

	if (Auth::attempt(['email' => $request['email'],'password' => $request['password'] ]))
	{

		return redirect()-> route ('user.profile');

	}

	return 	redirect()->back();
}

public function getProfile() {
	if (Auth::check()){
		$id=Auth::user()->id;
		if ($id==1){
			$orders=Order::all();
				$orders->transform(function($order,$key){
	       		$order->cart=unserialize($order->cart);
	       		return $order;
	       	});
				 return view('user.profile',['orders'=> $orders]);
		}
	       	$orders=Auth::user()->orders;
	       	$orders->transform(function($order,$key){
	       		$order->cart=unserialize($order->cart);
	       		return $order;
	       	});
	      ## $orders=Order::all();
	    	##foreach ($orders as $order) {
	    		##dd($order->cart);
		##	}

	       	 return view('user.profile',['orders'=> $orders]);
	 }
	 
	 
 return redirect()->route('user.signin');


}
public function getLogout(){

Auth::logout();
return redirect()->route ('producto.index');
}


}