<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Order;
use App\Models\Categoria;
use Session;
use App\Cart;
use Auth;

class ProductoController extends Controller
{
	public function getIndex(){

		$productos=Producto::all();

		return view('shop.index',['productos'=> $productos]);

	}
    public function getIndexbyCat(Request $request,$catid){

        $productos=Producto::all();

        return view('shop.index',['productos'=> $productos]);

    }

    public function getCheckout(){
       if (Session::has('cart')){
            $oldCart=Session::get('cart');
            $cart= new Cart($oldCart);
            $total=$cart -> totalPrice;
            return view ('shop.checkout',['total'=> $total]);

        }



        return view('shop.checkout',['total'=> 0]);

    }
    public function deleteCart(){

         Session::forget('cart');
         return redirect()->route ('producto.index');        
    }
public function postCheckout(Request $request){
       if (Session::has('cart')){
            $oldCart=Session::get('cart');
            $cart= new Cart($oldCart);
           $nombreCliente=$request['formNombreInput'];
           $direccionCliente=$request['formDireccionInput'];
           $TarjetaNombreCliente=$request['formNombreTarjetaInput'];
           $NumeroTarjetaCliente=$request['formNumTarjetaInput'];
           $total=$cart -> totalPrice;

           if (Auth::check()){

           $id=Auth::user()->id;
            
           }
           else{

            $id=1;
           }
           
           $orden=new Order([
            'total' => $total,
            'direccionCliente' => $direccionCliente ,
            'numeroTarjeta'=>  $NumeroTarjetaCliente,
            'nombreTarjeta'=>  $TarjetaNombreCliente,
            'cart' => serialize($cart),
            'user_id' => $id

            ]);
           $orden->save();

           

           #aqui guardar informacion y orden de compra


            Session::forget('cart');
            return redirect()->route ('producto.index');

        }
                return view('shop.shoppingCart');

}

    public function getWelcome(){


        return view('shop.welcome');

    }
		public function getEditarProducto(){

		$categorias=Categoria::all();
		return view('shop.editarProducto',['categorias'=> $categorias]);

	}

    public function postEditarProducto(){


    return redirect()->route('producto.index');

  }
    
    public function getAddtoCart(Request $request,$id){
    	$producto =Producto::find($id);
    	$oldCart = Session::has('cart') ? Session::get('cart') : null;

    	$cart = new Cart($oldCart);
    	
    	$cart -> add($producto, $producto -> id,$cart);
    	
    	$request->session()->put('cart',$cart);
    	#dd($request->session()->get('cart'));
    	return redirect()->route('producto.index');
    }
    public function getCart(){

    		if(!Session::has('cart')){

    			return view('shop.shoppingCart',['productos'=>null]);
    		}
    		$oldCart= Session::get('cart');
    		$cart = new Cart($oldCart);
    		return view('shop.shoppingCart',
    			['productos' => $cart->items,'totalPrice' => $cart->totalPrice ]);
    }
    
}
